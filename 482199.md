刷新 25 回复 377 关注

举报只看`#482199` **精品**3月之前 9-16 23:58:17

[#274045](https://web.thuhole.com/##274045)



dz来发布新版本了！介于旧洞过长且有许多过时内容，因此开新洞发布~



**教参平台爬虫**：自动下载书籍每一页的原图，生成PDF，免登录。



GitHub仓库：https://github.com/i207M/reserves-lib-tsinghua-downloader，欢迎Star~



`#2524760` 3月之前 9-16 23:59:52

[洞主]

### 下载地址



v2.1.1



[Windows](https://cdn.jsdelivr.net/gh/i207m/reserves-downloader-website@delivr/v2.1.1/downloader-windows-py3.9.zip)



[MacOS](https://cdn.jsdelivr.net/gh/i207m/reserves-downloader-website@delivr/v2.1.1/downloader-macos-py3.9.zip)



`#2524786` 3月之前 9-17 0:01:34

[洞主]

### 使用说明



运行`downloader`，输入网站“`阅读全文`”之下的链接地址（如图中标黄的位置）。程序会自动爬取当前章节以下的所有章节。



程序会将图片保存在`./clawed`下，并自动生成PDF。



**此程序无需登录即可使用**。

[![https://i.thuhole.com/he/hecnsgggfqi3saiamfdmzbyl22wlp2xg.jpeg](482199.assets/hecnsgggfqi3saiamfdmzbyl22wlp2xg.jpeg)](https://i.thuhole.com/he/hecnsgggfqi3saiamfdmzbyl22wlp2xg.jpeg)

`#2524794` 3月之前 9-17 0:02:13

[洞主]

### MacOS



MacOS用户可能无法直接运行下载的`downloader`，是因为它没有被标记为“可执行的”。



解决方法：在终端中进入`downloader`文件所在的文件夹`./dist`，执行`chmod +x downloader`命令。有关此命令的更多帮助请参阅[Apple](https://support.apple.com/zh-cn/guide/terminal/apdd100908f-06b3-4e63-8a87-32e71241bab4/mac)。



MacOS上的第一次启动可能会有点慢。



`#2524808` 3月之前 9-17 0:02:48

[洞主]

### Q&A



**Q:** 图片压缩的`quality ratio`怎样设置？



A: 范围[1, 96]：其中96为不压缩，[1, 95]从最差到最佳。



**Q:** 运行报错`Cookie Required`，怎么办？



A: 经测试，绝大部分教参无需`cookie`即可访问。少数教参需要`cookie`进行身份验证，请将网站`cookie`中，`.ASPXAUTH`和`ASP.NET_SessionId`的值依次写入`cookie.txt`中，每行一个。（我将会完善获取网站`cookie`的相关教程。若急需，请与我发邮件）



**Q:** 下载的章节不全？



A: 这是因为此图书的目录编号不连续。请再次运行程序并输入下一位置的章节链接。通常不会出现此情况。



**Q:** 分享一点高级玩法？



A: 使用学校提供的正版福昕编辑器可以进行OCR文字识别。



------



*仅供Python语言的学习参考，请勿用于非法用途！*



`#2524849` 3月之前 9-17 0:05:16

[洞主] v2.1.1更新内容：



- 

- 更好的命令行交互

  - 

  - 下载过程中会显示进度
  - 

  - 下载完毕之后，程序不会自动关闭了
  - 

  

- 

- 修复了某些PDF页码混乱的问题

- 



（建议使用旧版的同学升级一下，从上次发布到现在更新了比较多）



`#2524884` 3月之前 9-17 0:06:58

[洞主] 欢迎在GitHub上或在树洞里反馈~



记住一定不要传播本程序及电子书。



`#2525013` 3月之前 9-17 0:15:09

[洞主] Re 洞主: MacOS的解决方法中多加了`./dist`字段。现重写如下：



解决方法：在终端中进入`downloader`文件所在的文件夹，执行`chmod +x downloader`命令。有关此命令的更多帮助请参阅[Apple](https://support.apple.com/zh-cn/guide/terminal/apdd100908f-06b3-4e63-8a87-32e71241bab4/mac)。



过程的截图已附上。

[![https://i.thuhole.com/oj/ojyngzzunokctoh53zjbigbpocd5dfo3.jpeg](482199.assets/ojyngzzunokctoh53zjbigbpocd5dfo3.jpeg)](https://i.thuhole.com/oj/ojyngzzunokctoh53zjbigbpocd5dfo3.jpeg)

`#2525493` 3月之前 9-17 0:46:00

[Alice] 谢谢dz



`#2527090` 3月之前 9-17 7:59:28

[Bob] dz必4.0



`#2527207` 3月之前 9-17 8:58:25

[Carol] 我试了一下macos应该也可以直接
`python3 downloader.py --url [url]`



`#2527268` 3月之前 9-17 9:11:56

[洞主] Re Carol: 是的，运行Python源代码肯定没问题



`#2527414` 3月之前 9-17 9:50:49

[洞主] 接到相关反馈，现不再提供本项目文件的分发。请停止使用、传播并删除。后果自负。



`#2529266` 3月之前 9-17 14:55:57

[Dave] 马了先



`#2535368` 3月之前 9-18 11:01:27

[Eve] Cy



`#2558875` 3月之前 9-21 11:31:15

[Francis] 求exe文件



`#2575493` 3月之前 9-23 18:04:37

[洞主] Re 洞主: 不在树洞分发可执行文件的原因是，有同学善意提醒我，使用人数过多可能会导致图书馆升级网站，甚至追责到我。目前可以在GitHub上下载文件。



然后，使用本工具是不需要登录的，UA和浏览器相同。因此使用它的安全性...还是由自己判断吧。



`#2588788` 3月之前 9-25 12:10:25

[Grace] Re 洞主: 求教洞主为什么这样子还是会报错呢，不是已经调成可以调用的程序了嘛ww

[![https://i.thuhole.com/sl/slk7cbsq7g6hbgtbjktrost6ul5gukdm.jpeg](482199.assets/slk7cbsq7g6hbgtbjktrost6ul5gukdm.jpeg)](https://i.thuhole.com/sl/slk7cbsq7g6hbgtbjktrost6ul5gukdm.jpeg)

`#3031764` 16天之前 11-25 19:59:08

[Hans] 除了book4以外的其他书都没办法跳出循环，猜测是claw文件跳出循环的条件发生变化了，求问dz怎么破



`#3076643` 10天之前 12-01 18:26:56

[Isabella] 好像挂了）



`#3082142` 9天之前 12-02 11:12:58

[Jason] 报错httpconnectionpool



`#3097666` 8天之前 12-04 0:16:06

[洞主] Re Hans: 确实，发现了这个问题，这几天有空的话我改一下



`#3097669` 8天之前 12-04 0:16:17

[洞主] 谢谢反馈



`#3097746` 8天之前 12-04 0:27:25

[Kate] 已经下载一万多页了，根本停不下来



`#3108179` 6天之前 12-05 11:21:49

[洞主]

### NOTICE



2021年末，学校升级相关接口，旧脚本会出现死循环的问题。



借此机会，本项目将移交至一个小组维护。请移步 [libthu/reserves-lib-tsinghua-downloader](https://github.com/libthu/reserves-lib-tsinghua-downloader)，本仓库今后将不再更新。在新仓库中，应已完成对接口的升级。



谢谢大家的支持！



`#3108615` 6天之前 12-05 12:22:33

[Louis] 上不去github的求蹲一个下载链接qwq



 插入图片

 预览 发表