刷新 16 回复 249 关注

举报只看`#315389` **精品**7月之前 5-07 0:23:16

分享一下五一假期在天津吃的美食，洞友们可以去打卡哟！
（因为住的近，所以这些店铺基本在五大道/滨江道周边
天津的刨冰一定要吃！传统的老味儿刨冰（就是冰单独刨，然后在上面加料的那种）推荐铁真刨冰王的红豆炼乳冰山、老黑刨冰的老味儿刨冰；改良派首推美肴冰品的至尊雪奶系列，真的是好吃又好看，奶香四溢呜呜呜，价格基本在10～20元
西开教堂附近有一家店叫冰菓铺子，他们家的冰田（在刨冰内塞了很多料，还有自制的雪糕）非常独特，安利一下！
正餐餐馆的话，推荐五大道的桂园餐厅，黑蒜籽牛肉粒、八珍豆腐是必点菜（图上的就是黑蒜子牛肉粒）我自己点的鸭黄焗南瓜、乌鱼蛋汤也非常nice！不过人均稍高，大概80元的亚子。餐厅上午11点才开始营业而且人超多，所以可以考虑早点去占座，不介意的话也可以拼桌w
五大道还有家回民餐厅，叫宏玉祥黄门脸，物美价廉支持外卖，不过老餐厅服务都会差一点，瑕不掩瑜嘛
此外五大道有家24h营业的餐厅叫买又大餐厅，云吞和云吞面都是高汤做的，牛肉饼金黄酥脆入口即化，绝绝子！
地方小吃推荐炸糕、锅巴菜和各种煎饼果子。炸糕可以去耳朵眼炸糕（是连锁店），比较甜；锅巴菜可以去五大道的陈记锅巴菜。煎饼果子其实大家做的都挺好吃，主要区别是面、酱料和加料，我吃的几家路边摊都是坚定的绿豆面+双蛋，津门十三张好像做的是纯小麦面的，接受度似乎更高些（

[![https://i.thuhole.com/ko/koutezxilqda6yzdrtoqfdv3se6mo6zw.jpeg](315389.assets/koutezxilqda6yzdrtoqfdv3se6mo6zw.jpeg)](https://i.thuhole.com/ko/koutezxilqda6yzdrtoqfdv3se6mo6zw.jpeg)

`#1602726` 7月之前 5-07 0:25:20

[Alice] 滨江道刨大人冰品yyds



`#1602758` 7月之前 5-07 0:28:04

[Bob] 天津人激动收藏



`#1602760` 7月之前 5-07 0:28:33

[Bob] *家住五大道的天津人激动收藏



`#1602763` 7月之前 5-07 0:29:04

[洞主] Re Alice: ！这家早有耳闻但是没排到日程里呜呜，下次一定去吃



`#1602837` 7月之前 5-07 0:35:54

[Carol] dz懂行哈哈哈哈哈 这几家都好吃！



`#1604260` 7月之前 5-07 10:46:41

[Dave] 天津人激动收藏+1



`#1604265` 7月之前 5-07 10:48:07

[Dave] btw天津煎饼果子各早点摊都差不多，但有一家南楼煎饼只在晚上营业却很火，日常排队，应该也比较好吃，有机会也可以去试试



`#1605525` 7月之前 5-07 14:49:06

[Eve] 之前去天津吃过一家烧卖 在网上好像挺有名的 味道很不错



`#1606734` 7月之前 5-07 18:55:24

[Francis] 刚从天津回来的Francis感觉炸糕不是特别好吃



`#1607222` 7月之前 5-07 20:10:30

[Grace] Re Eve: 柳州路烧麦？



`#1607330` 7月之前 5-07 20:29:29

[洞主] Re Dave: 是在五大道南边那家吗？dz当时因为太懒了所以没去，看来下次一定得去瞅瞅



`#1607341` 7月之前 5-07 20:31:59

[洞主] Re Francis: 炸糕确实褒贬不一，有很多友友都觉得有点油+很甜，不过dz觉得还好



`#1607375` 7月之前 5-07 20:39:09

[Grace] Re Dave: 其实并没有只在晚上营业，现在南楼也有分店



`#1607469` 7月之前 5-07 20:53:24

[Dave] Re Grace: 咦是吗，那还能一直有人排队hh



`#1609921` 7月之前 5-08 2:12:34

[洞主] 草原来已经是热榜第一吗
谢谢洞友们支持，dz给各位拜个早年



`#1613437` 7月之前 5-08 17:48:41

[Hans] 强推胖子餐厅



 插入图片

 预览 发表