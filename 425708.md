刷新 31 回复 676 关注

举报只看`#425708` **精品**4月之前 8-03 16:23:07

### 清华各类官方网站、电子信息平台整理合集



欢迎补充



#### 常用网站



信息门户（info）[http://info.tsinghua.edu.cn](http://info.tsinghua.edu.cn/)
网络学堂 https://learn.tsinghua.edu.cn/f/login
选课登录 [zhjwxk.cic.tsinghua.edu.cn/xklogin.do](http://zhjwxk.cic.tsinghua.edu.cn/xklogin.do)
校园网登录 [net.tsinghua.edu.cn/](http://net.tsinghua.edu.cn/)
**荷塘**雨课堂 https://pro.yuketang.cn/web
清华云盘 https://cloud.tsinghua.edu.cn/
清华邮箱 https://mails.tsinghua.edu.cn/
清华家园网 http://myhome.tsinghua.edu.cn/
**学堂在线**慕课平台 https://www.xuetangx.com/
**清华大学**慕课平台 http://tsinghua.xuetangx.com/#/
**清华大学**雨课堂 https://tsinghua.yuketang.cn/pro/portal/home/
清华OJ https://dsa.cs.tsinghua.edu.cn/oj/
研讨间预约系统 http://yxxj.lib.tsinghua.edu.cn/ClientWeb/xcus/ic2/Default.aspx
学生清华（校团委） https://student.tsinghua.edu.cn/doc/notice



#### 常用微信公众号



艾生权（清华大学学生会生活权益部）
荷塘雨课堂
海昇自助洗衣（紫荆公寓扫码洗衣）
清华大学医院（校医院）
清华小清心（清华大学学生心理发展指导中心）
清华大学（学校官方微信公众号）
清华大学清新时报（校内新闻媒体）
清华大学新清华学堂（演出信息）
清华大学学生会（通知信息）
清华家园网（后勤管理）
平安清华（清华大学保卫部）
乐学（清华大学学习与发展指导中心）



### 其他电子平台



微信小程序“清华紫荆”（紫荆码）
微信小程序“荷塘雨课堂”
企业微信“清华大学信息服务”



`#2198343` 4月之前 8-03 16:23:26

[Alice] 多谢多谢！



`#2198357` 4月之前 8-03 16:24:53

[Bob] 只有知道info，其他在上面都能找到



`#2198363` 4月之前 8-03 16:25:35

[Carol] 清华大学图书馆 [lib.tsinghua.edu.cn](http://lib.tsinghua.edu.cn/)
清华大学教学门户 [academic.tsinghua.edu.cn](http://academic.tsinghua.edu.cn/)



`#2198377` 4月之前 8-03 16:27:10

[Carol] 清华大学教参服务平台 http://reserves.lib.tsinghua.edu.cn/
信息化用户服务平台 [its.tsinghua.edu.cn](http://its.tsinghua.edu.cn/)



`#2198387` 4月之前 8-03 16:28:54

[洞主] 清华大学开源软件镜像站 https://mirrors.tuna.tsinghua.edu.cn/



`#2198393` 4月之前 8-03 16:29:48

[Dave] 清华大学小研在身边，发的很多生权信息对本科生也很有用



`#2198400` 4月之前 8-03 16:30:32

[Dave] 华清捷利（紫荆公寓洗鞋机）



`#2198409` 4月之前 8-03 16:32:30

[Dave] 图书馆座位预约：[seat.tsinghua.edu.cn](http://seat.tsinghua.edu.cn/)



`#2198449` 4月之前 8-03 16:38:31

[Eve] Re Bob: 代码托管服务[git.tsinghua.edu.cn](http://git.tsinghua.edu.cn/)，你要能在info上找到算我输



`#2198462` 4月之前 8-03 16:41:41

[Francis] Re Eve: 除了信院，别的院系大概率用不上这个网站



`#2198541` 4月之前 8-03 16:49:44

[洞主] Re Eve: 感谢



`#2198555` 4月之前 8-03 16:52:04

[洞主] 微信小程序“清华校园巴士”



`#2198727` 4月之前 8-03 17:14:47

[Grace] 谢谢dz



`#2198802` 4月之前 8-03 17:24:14

[Hans] Re Carol: 教参服务平台有什么用啊，cz竟然第一次听说



`#2199163` 4月之前 8-03 18:07:15

[Dave] 清华大学图书馆公众号（也可以约座位）



`#2199165` 4月之前 8-03 18:07:20

[Dave] Re Hans: 看电子教材



`#2199771` 4月之前 8-03 19:16:39

[Isabella] 体育场馆预约：[50.tsinghua.edu.cn](http://50.tsinghua.edu.cn/)



`#2199775` 4月之前 8-03 19:17:07

[Isabella] 蹲一个李兆基实验室预约方式，就那种能玩的木工的什么的能约吗？b



`#2200809` 4月之前 8-03 21:58:22

[Jason] 清华大学网络电视：[iptv.tsinghua.edu.cn](http://iptv.tsinghua.edu.cn/)
看奥运会很舒适，而且延迟比较低



`#2202798` 4月之前 8-04 2:16:13

[Kate] Re Isabella: 似乎直接去就行2333



`#2203256` 4月之前 8-04 9:18:16

[Louis] Re Jason: 居然还有这个



`#2205307` 4月之前 8-04 15:29:49

[Margaret] 清华后勤



`#2205357` 4月之前 8-04 15:40:08

[Nathan] 校园网登录早就不用net了，现在最正规的方法应该是[login.tsinghua.edu.cn](http://login.tsinghua.edu.cn/)



`#2205395` 4月之前 8-04 15:45:13

[Nathan] 此外还有信息系统管理的[id.tsinghua.edu.cn](http://id.tsinghua.edu.cn/)，可能主要用途是改密码吧



`#2205397` 4月之前 8-04 15:46:06

[Nathan] 大家真的不要再推广[net.tsinghua.edu.cn](http://net.tsinghua.edu.cn/)认证了，已经过时很久了…



`#2205411` 4月之前 8-04 15:48:11

[Nathan] 看到一个校外登录的[webvpn.tsinghua.edu.cn](http://webvpn.tsinghua.edu.cn/)还没被提到诶



`#2294391` 4月之前 8-17 0:00:18

[Olivia] 补充一个公众号:清华体育



`#2302957` 4月之前 8-18 11:08:18

[Dave] 补充一个[usereg.tsinghua.edu.cn](http://usereg.tsinghua.edu.cn/)，可以用来看联网情况/改secure密码/修改允许联网设备数等



`#2340439` 4月之前 8-23 17:46:29

[Paul] 研读间网站登录



`#2343283` 4月之前 8-24 0:51:17

[Queen] [yjsy.cic.tsinghua.edu.cn](http://yjsy.cic.tsinghua.edu.cn/)



`#2489720` 3月之前 9-12 13:41:03

[Richard] h



 插入图片

 预览 发表